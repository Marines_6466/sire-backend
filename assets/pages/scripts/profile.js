var ProfileComponents = function () {

    return {
        //main function to initiate the module
        initDialStep: function () {
            //knob does not support ie8 so skip it
            if (!jQuery().knob || App.isIE8()) {
                return;
            }

            // general dialStep            
            $("#dialStep").knob({
                width: 250,
                height: 250,
                bgColor: '#5c5c5c',
                fgColor: '#74c37f',
                readOnly: true,
                displayInput: false
            });

            animateChart('dialStep', 0, 25);

            /* Update circle value when input.animate changes */
            $("#dialStep").change(function () {
                $(this).parents('.wrap-dial-step').find(".pie_value").text($(this).val());
            });

            /* Animate chart from start_val to end_val */
            function animateChart (chart_id, start_val, end_val) {
                $({chart_value: start_val}).animate({chart_value: end_val}, {
                    duration: 1000,
                    easing: 'swing',
                    step: function () {
                        $('#' + chart_id).val(Math.ceil(this.chart_value)).trigger('change');
                    }
                });
            };  
        },
        
        init: function () {
            this.initDialStep();
        }

    };

}();

jQuery(document).ready(function() {    
   ProfileComponents.init(); 
});