var CampaignComponents = function () {

    return {
        //main function to initiate the module
        initDialStep: function () {
            //knob does not support ie8 so skip it
            if (!jQuery().knob || App.isIE8()) {
                return;
            }

            // general dialStep            
            $("#dialStepProgress").knob({
                width: 250,
                height: 250,
                bgColor: '#5c5c5c',
                fgColor: '#74c37f',
                readOnly: true,
                displayInput: false
            });

            animateChart('dialStep', 0, 25);

            /* Update circle value when input.animate changes */
            $("#dialStepProgress").change(function () {
                $(this).parents('.wrap-dial-step').find(".pie_value").text($(this).val());
            });

            /* Animate chart from start_val to end_val */
            function animateChart (chart_id, start_val, end_val) {
                $({chart_value: start_val}).animate({chart_value: end_val}, {
                    duration: 1000,
                    easing: 'swing',
                    step: function () {
                        $('#' + chart_id).val(Math.ceil(this.chart_value)).trigger('change');
                    }
                });
            };  
        },

        initHandleSelectTemplate: function () {
            $('.campaign-template').bind('click', function (e) {
                $('.campaign-template').removeClass('active').find('.campaign-template-content').hide();
                $(this).addClass('active').find('.campaign-template-content').show();
                
                var heightBoxInner = $(this).find('.inner-content').outerHeight();
                $(this).find('.campaign-template-content').height(heightBoxInner);
            });

            $(window).bind('load resize', function () {
                var $box = $('.campaign-template.active').find('.campaign-template-content');
                var $hBox = $('.campaign-template.active').find('.inner-content').outerHeight();
                calCulateHeightBox($box, $hBox);
            });

            function calCulateHeightBox ($box, $hBox) {
                $box.height($hBox);
            }
        },

        handleBootstrapSelect: function() {
            $('.bs-select').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check'
            });
        },

        handleAddMoreAnswerText: function () {
            var $fieldStr = '';            

            $fieldStr += '<div class="row row-small">';
                $fieldStr += '<div class="col-xs-8 col-sm-10">';
                    $fieldStr += '<div class="form-group">';
                        $fieldStr += '<input type="text" class="form-control" />';
                    $fieldStr += '</div>';
                $fieldStr += '</div>';
                $fieldStr += '<div class="col-xs-2 col-sm-1">';
                    $fieldStr += '<div class="form-group">';
                        $fieldStr += '<input type="text" class="form-control text-center" value="" />';
                    $fieldStr += '</div>';
                $fieldStr += '</div>';
                $fieldStr += '<div class="col-xs-2 col-sm-1">';
                    $fieldStr += '<div class="form-group">';
                        $fieldStr += '<button class="form-control delete-answer">';
                            $fieldStr += '<i class="fa fa-minus"></i>';
                        $fieldStr += '</button>';
                    $fieldStr += '</div>';
                $fieldStr += '</div>';
            $fieldStr += '</div>';

            $('.wrapper-answer-text').on('click', '.add-more-answer', function(event) {
                event.preventDefault();
                $(this).parents('.wrapper-answer-text').find('.field-anser-holder').append($fieldStr);
            });

            $('.field-anser-holder').on('click', '.delete-answer', function(event) {
                event.preventDefault();
                $(this).parents('.row.row-small').remove();
            });
        },

        handleAddAnotherMessage: function () {
            var $rendered = new EJS({url:'assets/pages/scripts/another-message.ejs'}).render();
            
            $('.wrap-box-another-mesage').on('click', '.another-prepend', function(event) {
                event.preventDefault();
                $(this).parents('.wrap-box-another-mesage').find('.wrap-item-message').prepend($rendered);
            });

            $('.wrap-box-another-mesage').on('click', '.another-append', function(event) {
                event.preventDefault();
                $(this).parents('.wrap-box-another-mesage').find('.wrap-item-message').append($rendered);
            });
        },
        
        init: function () {
            this.initDialStep();
            this.initHandleSelectTemplate();            
            this.handleAddMoreAnswerText();
            this.handleAddAnotherMessage();
            this.handleBootstrapSelect();
        }

    };

}();

jQuery(document).ready(function() {    
   CampaignComponents.init(); 

   $('.bs-select').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check'
            });
});